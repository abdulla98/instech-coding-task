namespace Services.Cover;

public class YachtStrategy: PremiumCalculator
{
	public const decimal YachtMultiplier = 1.1M;
	protected override decimal CoverTypeMultiplier => YachtMultiplier;
	protected override decimal EarlyPeriodDiscountMultiplier { get; } = 0.05M;
	protected override decimal ExtendedPeriodDiscountMultiplier { get; } = 0.08M;

	public YachtStrategy(decimal basePremium) : base(basePremium) { }
}