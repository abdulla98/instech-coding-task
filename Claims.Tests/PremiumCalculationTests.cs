using FluentAssertions;
using Services.Cover;
using Xunit;

namespace Claims.Tests;

public class PremiumRateTests
{
	// The daily rate is established at 1250.
	// Premium adjustments for vessel types are as follows: Yachts incur a premium increase of 10%, Passenger ships by 20%, Tankers by 50%, and all other categories see a 30% rise.
	// For the next 150 days, Yachts receive a 5% discount, whereas other vessel types benefit from a 2% reduction.
	// Beyond this, any additional days come with further discounts: an extra 3% off for Yachts, with other types enjoying a 1% discount.

    [Fact]
    public void Yacht_premium_increase_for_initial_period()
    {
        var initialRate = Cover.DailyPremiumBase * YachtStrategy.YachtMultiplier;
        var expectedRate = 29 * initialRate;
        
        var cover = new Cover(CalculateDate(0), CalculateDate(29), CoverType.Yacht);
        
        cover.Premium.Should().Be(expectedRate);
    }

    [Fact]
    public void Yacht_receives_discount_next_150_days()
    {
		var baseRate = Cover.DailyPremiumBase * YachtStrategy.YachtMultiplier;

        var initialPeriodRate = 30 * baseRate;
        var discountedRate = 60 * (baseRate * 0.95m); 
        
        var cover = new Cover(CalculateDate(0), CalculateDate(90), CoverType.Yacht);

        cover.Premium.Should().Be(initialPeriodRate + discountedRate);
    }

    [Fact]
    public void Yacht_additional_discount_after_180_days()
    {
        var baseRate = Cover.DailyPremiumBase * YachtStrategy.YachtMultiplier;

        var initialPeriodRate = 30 * baseRate;
        var discountedRate = 150 * (baseRate * 0.95m);
		var additionalDiscountedRate = 180 * (baseRate * 0.92m);
        
        var cover = new Cover(CalculateDate(0), CalculateDate(360), CoverType.Yacht);

        cover.Premium.Should().Be(initialPeriodRate + discountedRate + additionalDiscountedRate);
    }

   [Fact]
    public void Passenger_ship_premium_increase_for_initial_period()
    {
		var baseRate = Cover.DailyPremiumBase * PassengerShipStrategy.PassengerShipMultiplier;
		var expectedPremium = 14 * baseRate;  
		
		var cover = new Cover(CalculateDate(0), CalculateDate(14), CoverType.PassengerShip);
		
		cover.Premium.Should().Be(expectedPremium);
    }

    [Fact]
    public void Passenger_ship_receives_discount_next_150_days()
    {
		var baseRate = Cover.DailyPremiumBase * PassengerShipStrategy.PassengerShipMultiplier;
		var initialPeriodRate = 30 * baseRate;
        var discountedRate = 100 * (baseRate * 0.98m);  
		
		var cover = new Cover(CalculateDate(0), CalculateDate(130), CoverType.PassengerShip);
		
		cover.Premium.Should().Be(initialPeriodRate + discountedRate);
    }

	 [Fact]
    public void Passenger_ship_additional_discount_after_180_days()
    {
		var baseRate = Cover.DailyPremiumBase * PassengerShipStrategy.PassengerShipMultiplier;
		var initialPeriodRate = 30 * baseRate;
        var discountedRate = 150 * (baseRate * 0.98m);
		var additionalDiscountedRate = 40 * (baseRate * 0.97m);
		
		var cover = new Cover(CalculateDate(0), CalculateDate(220), CoverType.PassengerShip);
		
		cover.Premium.Should().Be(initialPeriodRate + discountedRate + additionalDiscountedRate);
    }

	[Fact]
    public void Tanker_premium_increase_for_initial_period()
    {
		var baseRate = Cover.DailyPremiumBase * TankerStrategy.TankerMultiplier;
		var expectedPremium = 20 * baseRate;  
		
		var cover = new Cover(CalculateDate(0), CalculateDate(20), CoverType.Tanker);
		
		cover.Premium.Should().Be(expectedPremium);
    }

	[Fact]
	public void Tanker_receives_discount_next_150_days()
	{
		var baseRate = Cover.DailyPremiumBase * TankerStrategy.TankerMultiplier;
		var initialPeriodRate = 30 * baseRate;
		var discountedRate = 100 * (baseRate * 0.98m);  
		
		var cover = new Cover(CalculateDate(0), CalculateDate(130), CoverType.Tanker);
		
		cover.Premium.Should().Be(initialPeriodRate + discountedRate);
	}

	[Fact]
	public void Tanker_additional_discount_after_180_days()
	{
		var baseRate = Cover.DailyPremiumBase * TankerStrategy.TankerMultiplier;
		var initialPeriodRate = 30 * baseRate;
		var discountedRate = 150 * (baseRate * 0.98m);
		var additionalDiscountedRate = 90 * (baseRate * 0.97m);
		
		var cover = new Cover(CalculateDate(0), CalculateDate(270), CoverType.Tanker);
		
		cover.Premium.Should().Be(initialPeriodRate + discountedRate + additionalDiscountedRate);
	}

	[Theory]
	[InlineData(CoverType.BulkCarrier)]
	[InlineData(CoverType.ContainerShip)]
	public void Cover_increase_for_initial_period(CoverType type)
	{
		var baseRate = Cover.DailyPremiumBase * 1.3M;
		var expectedPremium = 20 * baseRate;
		
		var cover = new Cover(CalculateDate(0), CalculateDate(20), type);
		
		cover.Premium.Should().Be(expectedPremium);
	}

	[Theory]
	[InlineData(CoverType.BulkCarrier)]
	[InlineData(CoverType.ContainerShip)]
	public void Bulk_and_conainer_receive_discount_next_150_days(CoverType type)
	{
		var baseRate = Cover.DailyPremiumBase * 1.3M;
		var initialPeriodRate = 30 * baseRate;
		var discountedRate = 60 * (baseRate * 0.98m);

		var cover = new Cover(CalculateDate(0), CalculateDate(90), type);

		cover.Premium.Should().Be(initialPeriodRate + discountedRate);
	}

	[Theory]
	[InlineData(CoverType.BulkCarrier)]
	[InlineData(CoverType.ContainerShip)]
	public void Bulk_and_conainer_additional_discount_after_180_days(CoverType type)
	{
		var baseRate = Cover.DailyPremiumBase * 1.3M;
		var initialPeriodRate = 30 * baseRate;
		var discountedRate = 150 * (baseRate * 0.98m);
		var additionalDiscountedRate = 180 * (baseRate * 0.97m);

		var cover = new Cover(CalculateDate(0), CalculateDate(360), type);

		cover.Premium.Should().Be(initialPeriodRate + discountedRate + additionalDiscountedRate);
	}

    private DateOnly CalculateDate(int daysFromToday)
    {
        return DateOnly.FromDateTime(DateTime.UtcNow.AddDays(daysFromToday));
    }
}