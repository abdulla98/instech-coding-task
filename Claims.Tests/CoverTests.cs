using FluentAssertions;
using FluentAssertions.Execution;
using Services.Cover;
using Xunit;

namespace Claims.Tests;

public class PolicyCoverValidityTests
{
    [Fact]
    public void Verfy_start_date_is_valid()
    {
        var yesterday = DateTime.UtcNow.AddDays(-1);
        var tomorrow = DateTime.UtcNow.AddDays(1);

        Action createCoverAction = () => new Cover(DateOnly.FromDateTime(yesterday), 
                                                   DateOnly.FromDateTime(tomorrow), 
                                                   CoverType.Tanker);

        createCoverAction.Should().Throw<ArgumentException>()
                         .WithMessage("*past*");
    }

    [Fact]
    public void Verify_insurance_period_not_exceeding_limit()
    {
        var today = DateTime.UtcNow;
        var nextYearPlusOneDay = DateTime.UtcNow.AddYears(1).AddDays(1);

        Action extendedCoverAction = () => new Cover(DateOnly.FromDateTime(today), 
                                                     DateOnly.FromDateTime(nextYearPlusOneDay), 
                                                     CoverType.Tanker);

        extendedCoverAction.Should().Throw<ArgumentException>()
                            .WithMessage("*1 year*");
    }

    [Fact]
    public void Successfully_create_Cover()
    {
        var start = DateTime.UtcNow;
        var end = DateTime.UtcNow.AddYears(1);

        var validCover = new Cover(DateOnly.FromDateTime(start), 
                                   DateOnly.FromDateTime(end), 
                                   CoverType.Tanker);

        using (var scope = new AssertionScope())
        {
            validCover.Should().NotBeNull("because a valid cover should be successfully instantiated");
            validCover.Type.Should().Be(CoverType.Tanker, "because the specified cover type is Tanker");
            validCover.StartDate.Should().Be(DateOnly.FromDateTime(start), "because that is the start date provided");
            validCover.EndDate.Should().Be(DateOnly.FromDateTime(end), "because that is the end date provided reflecting a 1-year period");
        }
    }
}