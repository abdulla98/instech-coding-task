using FluentAssertions;
using FluentAssertions.Execution;
using FluentAssertions.Extensions;
using Services.Claim;
using Services.Cover;
using Xunit;

namespace Claims.Tests;

public class InsuranceClaimValidationTests
{
    private readonly Cover _defaultCover;

    public InsuranceClaimValidationTests()
    {
        // Default cover object for testing
        _defaultCover = new Cover(DateOnly.FromDateTime(DateTime.UtcNow.AddMonths(1)),
                                  DateOnly.FromDateTime(DateTime.UtcNow.AddYears(1).AddMonths(-1)), 
                                  CoverType.Tanker);
    }

    [Theory]
    [InlineData(100_500)] 
    [InlineData(205_000)]
    public void Verify_damage_cost_exceeding_limit(decimal excessiveDamage)
    {
        Action action = () => new Claim(_defaultCover, DateTime.Now.AddMonths(2), "ExcessiveDamage", ClaimType.Collision, excessiveDamage);
        
        action.Should().Throw<ArgumentException>()
              .WithMessage("*DamageCost*exceed*");
    }

    [Fact]
    public void Assert_creation_date_outside_cover_period()
    {
        Action creationOutsideCoverPeriod = () => new Claim(_defaultCover, DateTime.UtcNow.AddMonths(-2), "OutOfCover", ClaimType.Collision, 5);
        
        creationOutsideCoverPeriod.Should().Throw<ArgumentException>()
                                   .WithMessage("*Cover Period*");
    }

    [Fact]
    public void Successfully_create_Claim_within_Cover_period()
    {
        var validClaim = new Claim(_defaultCover, DateTime.UtcNow.AddMonths(2), "ValidClaim", ClaimType.Collision, 50);
        
        using (new AssertionScope())
        {
            validClaim.Should().NotBeNull("because a claim was successfully created with valid parameters");
            validClaim.Name.Should().Be("ValidClaim", "because we assigned this name upon creation");
            validClaim.Type.Should().Be(ClaimType.Collision, "because the specified claim type was Collision");
            validClaim.DamageCost.Should().Be(50, "because this was the damage cost specified at creation");
            validClaim.Created.Should().BeCloseTo(DateTime.UtcNow.AddMonths(2), TimeSpan.FromMilliseconds(100), "because the claim creation date should match the input");
        }
    }
}